package com.example.davaleba_3

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText

class axali : AppCompatActivity() {
    private lateinit var hobi:EditText
    private lateinit var finish:Button
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_axali)

        hobi=findViewById(R.id.hobby)
        finish=findViewById(R.id.buttonfinish)

        var name:String?=""
        var age1:String?=""
        if(intent.extras != null){
            name=intent.extras?.getString("NAME")
        }
        if(intent.extras != null){
            age1=intent.extras?.getString("AGE")
        }
        finish.setOnClickListener {
            val hobbyvar=hobi.text.toString()
            val intent= Intent(this,Display::class.java)
            intent.putExtra("AGE",age1)
            intent.putExtra("NAME",name)
            intent.putExtra("HOBBY",hobbyvar)
            startActivity(intent)
        }
    }
}