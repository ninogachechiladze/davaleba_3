package com.example.davaleba_3

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView

class Display : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_display)
        findViewById<TextView>(R.id.textView).text=
            intent.extras?.getString("NAME","ლამზირა")
        findViewById<TextView>(R.id.textView2).text=
            intent.extras?.getString("AGE","17")
        findViewById<TextView>(R.id.textView3).text=
            intent.extras?.getString("HOBBY","ცურვა")

    }
}