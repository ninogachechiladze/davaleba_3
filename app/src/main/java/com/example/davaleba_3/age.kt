package com.example.davaleba_3

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText

class age : AppCompatActivity() {
    private lateinit var asaki:EditText
    private lateinit var btnnextAge:Button
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_age)

        asaki=findViewById(R.id.editTextAge)
        btnnextAge=findViewById(R.id.buttonage)

        var name:String? =""

        if(intent.extras != null){
            name=intent.extras?.getString("NAME")
        }

        btnnextAge.setOnClickListener {
            val agevar=asaki.text.toString()
            val intent=Intent(this,axali::class.java)
            intent.putExtra("AGE",agevar)
            intent.putExtra("NAME",name)
            startActivity(intent)
        }

    }
}