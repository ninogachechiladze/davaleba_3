package com.example.davaleba_3

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.content.Intent

class MainActivity : AppCompatActivity() {

    private lateinit var sakheli:EditText
    private lateinit var btnnext:Button
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        sakheli=findViewById(R.id.sakheli)
        btnnext=findViewById(R.id.button)

        btnnext.setOnClickListener {
            val name=sakheli.text.toString()
            val intent=Intent(this,age::class.java)
            intent.putExtra("NAME",name)
            startActivity(intent)
        }

    }

}